# openweather-rs

This crate will retrieve the _current_ weather from openweathermap.org and
return the data as the OpenWeather type.

## Usage

```rust
extern crate openweather_rs;
use openweather_rs::get_weather;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let weather = get_weather(
        "API_KEY".to_string(),
        "12345".to_string(),
        "imperial".to_string()
        ).await?;
    println!("{:?}:", weather);
    Ok(())
}
```

## OpenWeather Type

`get_weather()` will return an OpenWeather type, which has the following structure:

```shell
OpenWeather {
    coord: Coord {
        lat: 42.81,
        lon: -73.94,
    },
    weather: [
        Description {
            id: 804,
            main: "Clouds",
            description: "overcast clouds",
            icon: "04d",
        },
    ],
    base: "stations",
    main: Main {
        temp: 61.99,
        feels_like: 64.74,
        temp_min: 60.01,
        temp_max: 63.0,
        pressure: 1024.0,
        humidity: 95.0,
        sea_level: 0.0,
        grnd_level: 0.0,
    },
    visibility: 10000.0,
    wind: Wind {
        speed: 1.3,
        deg: 61.0,
        gust: 0.0,
    },
    rain: Precipitation {
        one_hour: 0.0,
        three_hour: 0.0,
    },
    snow: Precipitation {
        one_hour: 0.0,
        three_hour: 0.0,
    },
    clouds: Clouds {
        all: 98,
    },
    dt: 1599653221,
    sys: Sys {
        type: 3,
        id: 2003286,
        country: "US",
        sunrise: 1599647381,
        sunset: 1599693389,
    },
    timezone: -14400.0,
    id: 0,
    name: "Schenectady",
    cod: 200,
}
```
