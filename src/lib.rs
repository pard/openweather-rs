extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

pub mod open_weather_types;

use open_weather_types::*;
use reqwest::Client;

type Error = Box<dyn std::error::Error>;
type Result<T, E = Error> = std::result::Result<T, E>;

pub async fn get_weather(appid: String, zip: String, unit: String) -> Result<OpenWeather, Error> {
    let client = Client::new();
    let uri = format!(
        "https://api.openweathermap.org/data/2.5/weather?appid={}&zip={}&units={}",
        appid,
        zip,
        unit
        );

    let req = client.get(&uri);
    let res = req.send().await?;
    let body = res.text().await?;
    let weather: OpenWeather = serde_json::from_str(&body)?;

    Ok(weather)

}

//#[cfg(test)]
//mod tests {
//    #[test]
//    fn it_works() {
//        assert_eq!(2 + 2, 4);
//    }
//}
